from django.contrib import admin
from .models  import ProductModel,OrdersModel,OrderItemsModel

# Register your models here.
class ProductAdmin(admin.ModelAdmin):
    list_display = ['Title','Description','Image_link','price','Created_At','Updated_At']

admin.site.register(ProductModel,ProductAdmin)

class OrderAdmin(admin.ModelAdmin):
    list_display = ['Total','created_at','updated_at','status','mode_of_payment']

admin.site.register(OrdersModel,OrderAdmin)

class OrderItemsAdmin(admin.ModelAdmin):
    list_display = ['Order','Product','price']

admin.site.register(OrderItemsModel,OrderItemsAdmin)