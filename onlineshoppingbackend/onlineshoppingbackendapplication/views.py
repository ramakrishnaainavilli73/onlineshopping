from .models import ProductModel, OrdersModel, OrderItemsModel
from .serializers import ProductSerializer, OrderSerializer, OrderItemsSerializer, UserSerializer
from rest_framework import viewsets
from django.shortcuts import render
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.models import User
from rest_framework.decorators import api_view


class ProductViewSet(viewsets.ModelViewSet):
    queryset = ProductModel.objects.all()
    serializer_class = ProductSerializer
    # permission_classes = [IsAuthenticated]
    # authentication_classes = (TokenAuthentication,)


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    # authentication_classes = [IsAuthenticated]
    # authentication_classes = (TokenAuthentication)
    def get(self,request,id):
        return self.list(request,id)

    def post(self, request):
        return render(self.createuser(request))

# class ProductDetails(generics.RetrieveUpdateDestroyAPIView):
#     def get_queryset(self):
#         return ProductModel.objects.filter(user=self.request.user)
#
#     serializer_class = ProductSerializer
#     # authentication_classes = [BasicAuthentication]
#     permission_classes = [IsAuthenticated]
