from django.apps import AppConfig


class OnlineshoppingbackendapplicationConfig(AppConfig):
    name = 'onlineshoppingbackendapplication'
