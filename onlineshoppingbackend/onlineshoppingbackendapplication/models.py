from django.db import models
from django.contrib.auth.models import User

# Create your models here.
# class Register(models.Model):
#     username = models.CharField(max_length=20)
#     password = models.CharField(max_length=12)
#     confirm_password = models.CharField(max_length=12,null=True)
#     email= models.EmailField()


class ProductModel(models.Model):
     Title = models.CharField(max_length=200)
     Description = models.TextField(max_length=300)
     Image_link = models.URLField()
     price = models.DecimalField(max_digits=10,decimal_places=2)
     Created_At = models.DateField(auto_now_add=True)
     Updated_At = models.DateField(auto_now=True)

     def __str__(self):

        return f"{self.id}-{self.Title}-{self.Description}-{self.price}"

class OrdersModel(models.Model):
    type_new = 'new'
    type_paid = 'paid'
    choices = (
        (type_new, 'New'),
        (type_paid, 'Paid')
    )

    type_cash = 'cash'
    Choices = (
        (type_cash,'Cash'),
    )
    user = models.ForeignKey(User,on_delete=models.CASCADE,null=True,blank=True)
    Total = models.DecimalField(max_digits=10,decimal_places=2)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)
    status = models.CharField(max_length=20,choices=choices)
    mode_of_payment = models.CharField(max_length=20,choices=Choices,default=type_cash)
    def __str__(self):
        return f"{self.created_at}-{self.updated_at}-{self.status}-{self.mode_of_payment}"

class OrderItemsModel(models.Model):

    Order = models.ForeignKey(OrdersModel,on_delete=models.CASCADE)
    Product = models.ForeignKey(ProductModel, on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=10,decimal_places=2,null=True)
    def __str__(self):
        return f"{self.price}"
