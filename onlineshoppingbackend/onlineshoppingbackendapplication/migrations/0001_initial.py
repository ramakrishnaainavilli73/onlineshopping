# Generated by Django 3.1.4 on 2021-01-04 06:11

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Title', models.CharField(max_length=200)),
                ('Description', models.TextField(max_length=300)),
                ('Image_link', models.URLField()),
                ('price', models.DecimalField(decimal_places=2, max_digits=10)),
                ('Created_At', models.DateField(auto_now_add=True)),
                ('Updated_At', models.DateField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='OrdersModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Total', models.DecimalField(decimal_places=2, max_digits=10)),
                ('created_at', models.DateField(auto_now_add=True)),
                ('updated_at', models.DateField(auto_now=True)),
                ('status', models.BooleanField(choices=[('new', 'New'), ('paid', 'Paid')])),
                ('mode_of_payment', models.BooleanField(choices=[('cash', 'Cash'), ('paytm', 'Paytm'), ('card', 'Card')], default='cash')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
