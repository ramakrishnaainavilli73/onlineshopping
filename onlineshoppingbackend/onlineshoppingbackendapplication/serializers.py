from rest_framework import serializers
from django.contrib.auth.models import User
from rest_framework.authtoken.views import Token

from onlineshoppingbackendapplication.models import ProductModel,OrdersModel,OrderItemsModel

# class RegisterSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Register
#         fields = ('username','password','email')
#     def cleaneddata(self):
#          username =
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'password']

        extra_kwargs = {'password':{
            'write_only':True,
            'required':True
        }}

    def createuser(self, validated_data):
        user = User.objects.create_user(**validated_data)
        Token.objects.create(user=user)
        return user

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductModel
        fields = ('Title','Description,','price','Created_At','Updated_At')


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrdersModel
        fields = ('Total', 'created_at','updated_at','status','mode_of_payment','user')
class OrderItemsSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItemsModel
        fields = '__all__'